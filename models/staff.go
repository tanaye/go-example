package models

type UserPlayer struct {
	UserId   int
	Username string
	Role     string
}
