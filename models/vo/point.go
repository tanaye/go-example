package vo

import (
	"github.com/gin-gonic/gin"
	"go-example/models"
)

type Point struct {
	Scores int `json: scores`
	UserId int
}

func BindPointInfo(context *gin.Context) (Point, error) {
	var point Point
	err := context.BindJSON(&point)
	point.UserId = context.Keys["users"].(*models.UserPlayer).UserId
	return point, err
}
