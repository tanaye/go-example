package vo

import "github.com/gin-gonic/gin"

type Advertising struct {
	Id       string `json: id`
	Title    string `json: title`
	Ordering int    `json: ordering`
}

func BindAdvertisingInfo(context *gin.Context) (Advertising, error) {
	var advertising Advertising
	err := context.BindJSON(&advertising)
	return advertising, err
}
