package vo

import "github.com/gin-gonic/gin"

type User struct {
	Username string `json: username`
	Password string `json: password`
	Role     string `json: role`
	Position string `json: position`
}

func BindUserInfo(context *gin.Context) (User, error) {
	var user User
	err := context.BindJSON(&user)
	return user, err
}
