package models

type Advertising struct {
	Id       int
	Title    string
	Ordering int
}
