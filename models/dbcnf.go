package models

type Dbconf struct {
	Engine   string
	Server   string
	Port     string
	User     string
	Password string
	Database string
}
