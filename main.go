package main

import (
	"fmt"
	"go-example/router"
)

func main() {
	fmt.Printf("Go API Example\n")
	router.Register()
}
