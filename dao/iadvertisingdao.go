package dao

import (
	"go-example/models/vo"
)

type IAdvertisingDao interface {
	GetListAdvs() ([]string, error)

	CreateAdvertising(advertising vo.Advertising) (bool, error)

	ChangeStatus(id int, status int) (bool, error)

	GetDetailAdvs() ([]vo.Advertising, error)

	UpdateOrderingAdv(advertising vo.Advertising) (bool, error)
}
