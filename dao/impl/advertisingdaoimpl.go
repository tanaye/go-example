package impl

import (
	"go-example/common"
	"go-example/models/vo"
)

type AdvertisingDaoImpl struct {
	common.Connection
}

func (dao AdvertisingDaoImpl) GetListAdvs() ([]string, error) {
	advs := make([]string, 0)

	query := "SELECT title FROM advertising WHERE status = ? ORDER BY ordering "

	rows, err := dao.Connection.Select(query, common.STATUS_APPROVE)

	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var adv string
		err := rows.Scan(&adv)
		if err != nil {
			return nil, err
		}
		advs = append(advs, adv)
	}

	return advs, nil

}

func (dao AdvertisingDaoImpl) CreateAdvertising(advertising vo.Advertising) (bool, error) {
	query := "INSERT INTO advertising(title, ordering) SELECT ?, COALESCE((MAX(ordering) + 1), 1) FROM advertising"
	id, err := dao.Connection.Insert(query, advertising.Title)
	return id > 0, err
}

func (dao AdvertisingDaoImpl) ChangeStatus(id int, status int) (bool, error) {
	query := "UPDATE  advertising SET status = ? WHERE id = ? "
	row, err := dao.Connection.Update(query, status, id)
	return row > 0, err
}

func (dao AdvertisingDaoImpl) GetDetailAdvs() ([]vo.Advertising, error) {
	advs := make([]vo.Advertising, 0)

	query := "SELECT id, title, ordering FROM advertising WHERE status = ? ORDER BY ordering "

	rows, err := dao.Connection.Select(query, common.STATUS_APPROVE)

	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var adv vo.Advertising
		err := rows.Scan(&adv.Id, &adv.Title, &adv.Ordering)
		if err != nil {
			return nil, err
		}
		advs = append(advs, adv)
	}

	return advs, nil
}

func (dao AdvertisingDaoImpl) UpdateOrderingAdv(advertising vo.Advertising) (bool, error) {
	query := "UPDATE  advertising SET ordering = ? WHERE id = ? "
	row, err := dao.Connection.Update(query, advertising.Ordering, advertising.Id)
	return row > 0, err
}
