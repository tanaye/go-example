package impl

import (
	"go-example/common"
	"go-example/models"
	"go-example/models/vo"
)

type PointDaoImpl struct {
	common.Connection
}

func (dao PointDaoImpl) GetTopPoint() ([]models.Point, error) {
	points := make([]models.Point, 0)

	query := "SELECT u.id as userId, u.username as username, us.scores as scorces FROM users_scores us " +
		"INNER JOIN users u ON u.id = us.users_id " +
		"ORDER BY us.scores LIMIT 10"

	rows, err := dao.Connection.Select(query)

	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var point models.Point
		err := rows.Scan(&point.UserId, &point.UserName, &point.Scores)
		if err != nil {
			return nil, err
		}
		points = append(points, point)
	}

	return points, nil

}

func (dao PointDaoImpl) CreatePoint(point vo.Point) (bool, error) {
	query := "INSERT INTO users_scores (users_id, scores) VALUES (?, ?) ON DUPLICATE KEY UPDATE scores = GREATEST(scores, VALUES(scores))"
	row, err := dao.Connection.Update(query, point.UserId, point.Scores)
	return row > 0, err
}
