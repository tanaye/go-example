package impl

import (
	"go-example/common"
	"go-example/models"
	"go-example/models/vo"
)

// UserDaoImpl : Implementation of IStaffDao
type UserDaoImpl struct {
	common.Connection
}

// GetAll : Get all staff in database
func (dao UserDaoImpl) GetAllUser() ([]models.UserPlayer, error) {

	staffs := make([]models.UserPlayer, 0)

	query := " SELECT staffId, staffName, position, role FROM staffs WHERE role = ?; "

	rows, err := dao.Connection.Select(query, "STAFF")

	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var staff models.UserPlayer
		err := rows.Scan(&staff.UserId, &staff.Username, &staff.Role)
		if err != nil {
			return nil, err
		}
		staffs = append(staffs, staff)
	}

	return staffs, nil

}

func (dao UserDaoImpl) CreateUser(user vo.User) (int64, error) {
	query := "INSERT INTO users(username, password, type) VALUES (?, md5(?), ?) "
	return dao.Connection.Insert(query, user.Username, user.Password, user.Type)
}

func (dao UserDaoImpl) FindUser(user vo.User) (*models.UserPlayer, error) {

	query := " SELECT u.id AS userId , u.username AS username, IF(ut.id = 1, 'ADMIN', 'PLAYER') as role, u.type " +
		"FROM users u " +
		"INNER JOIN users_preferences up ON u.id = up.user_id " +
		"INNER JOIN users_types ut ON ut.id = up.user_type_id " +
		"WHERE username = ? AND PASSWORD = md5(?) "

	rows, err := dao.Connection.Select(query, user.Username, user.Password)

	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var staff models.UserPlayer
		err := rows.Scan(&staff.UserId, &staff.Username, &staff.Role, &staff.Type)
		if err != nil {
			return nil, err
		}
		return &staff, nil
	}

	return nil, nil

}

func (dao UserDaoImpl) CreateUserPreferences(userId int64, userType int) (int64, error) {
	query := "INSERT INTO users_preferences(user_id, user_type_id) VALUES (?, ?) "
	return dao.Connection.Insert(query, userId, userType)
}
