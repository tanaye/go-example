package dao

import (
	"go-example/models"
	"go-example/models/vo"
)

type IUserDao interface {
	GetAllUser() ([]models.UserPlayer, error)

	CreateUser(u vo.User) (int64, error)

	FindUser(user vo.User) (*models.UserPlayer, error)

	CreateUserPreferences(userId int64, userType int) (int64, error)
}
