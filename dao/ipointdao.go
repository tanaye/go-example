package dao

import (
	"go-example/models"
	"go-example/models/vo"
)

type IPointDao interface {
	GetTopPoint() ([]models.Point, error)

	CreatePoint(point vo.Point) (bool, error)
}
