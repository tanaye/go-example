package controller

import (
	"github.com/gin-gonic/gin"
	"go-example/common"
	"go-example/models/vo"
	"go-example/service"
	"go-example/service/impl"
	"net/http"
	"strconv"
)

type AdvertisingController struct {
	AdvService service.IAdvertisingService
}

func NewAdvertisingController() *AdvertisingController {
	return &AdvertisingController{AdvService: impl.NewAdvertisingService()}
}

func (controller *AdvertisingController) GetAdvs(context *gin.Context) {
	advs, err := controller.AdvService.GetListAdvs()

	if err != nil {
		context.JSON(http.StatusInternalServerError,
			&common.Response{Code: http.StatusInternalServerError,
				Data: map[string]interface{}{
					"error": err.Error(),
				}})
		return
	}

	context.JSON(http.StatusOK,
		&common.Response{Code: http.StatusOK,
			Data: map[string]interface{}{
				"advs": advs,
			}})
}

func (controller *AdvertisingController) CreateAdvertising(context *gin.Context) {
	adv, err := vo.BindAdvertisingInfo(context)

	if err != nil {
		context.JSON(http.StatusInternalServerError,
			&common.Response{
				Code: http.StatusInternalServerError,
				Data: map[string]interface{}{
					"error": err.Error(),
				}})
	}

	if adv.Title != "" {
		if _, err := controller.AdvService.CreateAdvertising(adv); err != nil {
			context.JSON(http.StatusInternalServerError,
				&common.Response{
					Code: http.StatusInternalServerError,
					Data: map[string]interface{}{
						"error": err.Error(),
					}})
		} else {
			context.JSON(http.StatusCreated,
				&common.Response{
					Code: http.StatusOK,
					Data: map[string]interface{}{
						"success": "Create advertising success !",
					}})
		}
	} else {
		context.JSON(http.StatusBadRequest,
			&common.Response{
				Code: http.StatusBadRequest,
				Data: map[string]interface{}{
					"error": "Title is empty",
				}})
	}

}

func (controller *AdvertisingController) DeleteAdvertising(context *gin.Context) {
	if id, err := strconv.Atoi(context.Query("id")); err == nil {
		if _, err := controller.AdvService.DeleteAdvertising(id); err == nil {
			context.JSON(http.StatusOK,
				&common.Response{
					Code: http.StatusOK,
					Data: map[string]interface{}{
						"success": "Delete advertising success !",
					}})
		} else {
			context.JSON(http.StatusBadRequest,
				&common.Response{
					Code: http.StatusBadRequest,
					Data: map[string]interface{}{
						"error": err.Error(),
					}})
		}
	} else {
		context.JSON(http.StatusBadRequest,
			&common.Response{
				Code: http.StatusBadRequest,
				Data: map[string]interface{}{
					"error": err.Error(),
				}})
	}

}

func (controller *AdvertisingController) GetDetailAdvs(context *gin.Context) {
	if data, err := controller.AdvService.GetDetailAdvs(); err != nil {
		context.JSON(http.StatusInternalServerError,
			&common.Response{
				Code: http.StatusInternalServerError,
				Data: map[string]interface{}{
					"error": err.Error(),
				}})
	} else {
		context.JSON(http.StatusOK,
			&common.Response{
				Code: http.StatusOK,
				Data: map[string]interface{}{
					"advs": data,
				}})
	}
}

func (controller *AdvertisingController) UpdateOrderingAdv(context *gin.Context) {
	adv, err := vo.BindAdvertisingInfo(context)

	if err != nil {
		context.JSON(http.StatusInternalServerError,
			&common.Response{
				Code: http.StatusInternalServerError,
				Data: map[string]interface{}{
					"error": err.Error(),
				}})
	}

	if _, err := controller.AdvService.UpdateOrderingAdv(adv); err != nil {
		context.JSON(http.StatusInternalServerError,
			&common.Response{
				Code: http.StatusInternalServerError,
				Data: map[string]interface{}{
					"error": err.Error(),
				}})
	} else {
		context.JSON(http.StatusOK,
			&common.Response{
				Code: http.StatusOK,
				Data: map[string]interface{}{
					"success": "Update advertising success !",
				}})
	}
}
