package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/go-sql-driver/mysql"
	"go-example/common"
	"go-example/models/vo"
	"go-example/service"
	"go-example/service/impl"
	"net/http"
)

type AuthController struct {
	AuthService service.IAuthService
	UserService service.IUserService
}

func NewAuthController() *AuthController {
	return &AuthController{
		AuthService: impl.NewAuthService(),
		UserService: impl.NewUserService()}
}

func (controller *AuthController) LoginHandler(context *gin.Context) {
	middle := controller.AuthService.CreateMiddleware()
	middle.LoginHandler(context)
}

func (controller *AuthController) RefreshHandler(context *gin.Context) {
	middle := controller.AuthService.CreateMiddleware()
	middle.RefreshHandler(context)
}

func (controller *AuthController) Register(context *gin.Context) {
	user, err := vo.BindUserInfo(context)

	if err != nil {
		context.JSON(http.StatusInternalServerError,
			&common.Response{
				Code: http.StatusInternalServerError,
				Data: map[string]interface{}{
					"error": err.Error(),
				}})
	}

	if user.Username != "" && user.Password != "" {
		if _, err := controller.UserService.CreateUser(user); err != nil {
			me, ok := err.(*mysql.MySQLError)
			if !ok {
				context.JSON(http.StatusInternalServerError,
					&common.Response{
						Code: http.StatusInternalServerError,
						Data: map[string]interface{}{
							"error": err.Error(),
						}})
			}

			if me.Number == 1062 {
				context.JSON(http.StatusBadRequest,
					&common.Response{
						Code: http.StatusBadRequest,
						Data: map[string]interface{}{
							"error": "Username already exists in a database. ",
						}})
			}
		} else {
			controller.AuthService.CreateJwt(user, context)
		}
	} else {
		context.JSON(http.StatusBadRequest,
			&common.Response{
				Code: http.StatusBadRequest,
				Data: map[string]interface{}{
					"error": "Username Or Password is empty",
				}})
	}

}
