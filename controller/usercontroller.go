package controller

import (
	"go-example/common"
	"go-example/models"
	"go-example/models/vo"
	"go-example/service"
	"go-example/service/impl"
	"net/http"

	"github.com/gin-gonic/gin"
)

// UserController : User controller
type UserController struct {
	UserService service.IUserService
}

// NewUserController : constructor new object
func NewUserController() *UserController {
	return &UserController{UserService: impl.NewUserService()}
}

//
func (controller *UserController) Login(context *gin.Context) {
	user, err := vo.BindUserInfo(context)

	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{})
	}

	staff, err := controller.UserService.FindUser(user)

	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{})
	}

	jwt, err := controller.UserService.CreateJwt(staff)

	if err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{})
	}

	context.JSON(http.StatusOK, gin.H{
		"data": jwt,
	})

}

// GetUsers : Get user have role staff
func (controller *UserController) GetUsers(c *gin.Context) {
	staffs, err := controller.UserService.GetAllUser()

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"data": make([]models.UserPlayer, 0),
		})
	}

	c.JSON(http.StatusOK, gin.H{
		"data": staffs,
	})
}

func (controller *UserController) GetMyInfo(c *gin.Context) {
	user := c.Keys["users"].(*models.UserPlayer)

	c.JSON(http.StatusCreated,
		&common.Response{
			Code: http.StatusOK,
			Data: map[string]interface{}{
				"data": user,
			}})
}
