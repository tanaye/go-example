package controller

import (
	"github.com/gin-gonic/gin"
	"go-example/common"
	"go-example/models/vo"
	"go-example/service"
	"go-example/service/impl"
	"net/http"
)

type PointController struct {
	PointService service.IPointService
}

func NewPointController() *PointController {
	return &PointController{PointService: impl.NewPointService()}
}

func (controller *PointController) GetTopPoint(context *gin.Context) {
	if data, err := controller.PointService.GetTopPoint(); err != nil {
		context.JSON(http.StatusInternalServerError,
			&common.Response{
				Code: http.StatusInternalServerError,
				Data: map[string]interface{}{
					"error": err.Error(),
				}})
	} else {
		context.JSON(http.StatusOK,
			&common.Response{
				Code: http.StatusOK,
				Data: map[string]interface{}{
					"points": data,
				}})
	}

}

func (controller *PointController) CreatePoint(context *gin.Context) {

	if point, err := vo.BindPointInfo(context); err != nil {
		context.JSON(http.StatusInternalServerError,
			&common.Response{
				Code: http.StatusInternalServerError,
				Data: map[string]interface{}{
					"error": err.Error(),
				}})
	} else if point.Scores < 0 {
		context.JSON(http.StatusBadRequest,
			&common.Response{
				Code: http.StatusBadRequest,
				Data: map[string]interface{}{
					"error": "Scores is empty or negative",
				}})
	} else if _, err := controller.PointService.Create(point); err != nil {
		context.JSON(http.StatusInternalServerError,
			&common.Response{
				Code: http.StatusInternalServerError,
				Data: map[string]interface{}{
					"error": err.Error(),
				}})
	} else {
		context.JSON(http.StatusCreated,
			&common.Response{
				Code: http.StatusOK,
				Data: map[string]interface{}{
					"success": "Create success",
				}})
	}
}
