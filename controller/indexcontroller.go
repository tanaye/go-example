package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// IndexController : Index controller
type IndexController struct {
}

func NewIndexController() *IndexController {
	return &IndexController{}
}

// Index : Index controller
func (controller *IndexController) Index(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"data": "Success",
	})
}
