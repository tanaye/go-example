package service

import (
	"go-example/models"
	"go-example/models/vo"
)

// IUserService : User service interface
type IUserService interface {
	GetAllUser() ([]models.UserPlayer, error)

	CreateUser(u vo.User) (int64, error)

	FindUser(user vo.User) (*models.UserPlayer, error)

	CreateJwt(staff *models.UserPlayer) (map[interface{}]interface{}, error)
}
