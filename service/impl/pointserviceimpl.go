package impl

import (
	"go-example/dao"
	"go-example/dao/impl"
	"go-example/models"
	"go-example/models/vo"
)

type PointServiceImpl struct {
	PointDao dao.IPointDao
}

func NewPointService() *PointServiceImpl {
	var pointDao dao.IPointDao = impl.PointDaoImpl{}
	return &PointServiceImpl{PointDao: pointDao}
}

func (service *PointServiceImpl) GetTopPoint() ([]models.Point, error) {
	return service.PointDao.GetTopPoint()
}

func (service *PointServiceImpl) Create(point vo.Point) (bool, error) {
	return service.PointDao.CreatePoint(point)
}
