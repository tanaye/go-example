package impl

import (
	"go-example/common"
	"go-example/dao"
	"go-example/dao/impl"
	"go-example/models/vo"
)

type AdvertisingServiceImpl struct {
	AdvDao dao.IAdvertisingDao
}

func NewAdvertisingService() *AdvertisingServiceImpl {
	var advDao dao.IAdvertisingDao = impl.AdvertisingDaoImpl{}
	return &AdvertisingServiceImpl{AdvDao: advDao}
}

func (adv *AdvertisingServiceImpl) GetListAdvs() ([]string, error) {
	return adv.AdvDao.GetListAdvs()
}

func (adv *AdvertisingServiceImpl) CreateAdvertising(advertising vo.Advertising) (bool, error) {
	return adv.AdvDao.CreateAdvertising(advertising)
}

func (adv *AdvertisingServiceImpl) DeleteAdvertising(id int) (bool, error) {
	return adv.AdvDao.ChangeStatus(id, common.STATUS_DELETE)
}

func (adv *AdvertisingServiceImpl) GetDetailAdvs() ([]vo.Advertising, error) {
	return adv.AdvDao.GetDetailAdvs()
}

func (adv *AdvertisingServiceImpl) UpdateOrderingAdv(advertising vo.Advertising) (bool, error) {
	return adv.AdvDao.UpdateOrderingAdv(advertising)
}
