package impl

import (
	"github.com/appleboy/gin-jwt"
	"github.com/gin-gonic/gin"
	"go-example/common"
	"go-example/dao"
	"go-example/dao/impl"
	"go-example/models"
	"go-example/models/vo"
	jwt2 "gopkg.in/dgrijalva/jwt-go.v3"
	"time"
)

type AuthServiceImpl struct {
	UserDao dao.IUserDao
}

func NewAuthService() *AuthServiceImpl {
	var userDao dao.IUserDao = impl.UserDaoImpl{}
	return &AuthServiceImpl{UserDao: userDao}
}

func (auth AuthServiceImpl) CreateMiddleware(roles ...string) jwt.GinJWTMiddleware {
	var middle = jwt.GinJWTMiddleware{
		Realm:            "Tanaye",
		SigningAlgorithm: "HS256",
		Key:              []byte(common.GetAppConfiguration()["privateKey"].(string)),
		Timeout:          time.Minute * time.Duration(common.GetAppConfiguration()["expire"].(float64)),
		MaxRefresh:       time.Hour,
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			if v, ok := data.(*models.UserPlayer); ok {
				return jwt.MapClaims{
					"id":   v.UserId,
					"iss":  v.Username,
					"role": v.Role,
				}
			}
			return jwt.MapClaims{}
		},
		IdentityHandler: func(claims jwt2.MapClaims) interface{} {
			return &models.UserPlayer{
				UserId:   int(claims["id"].(float64)),
				Username: claims["iss"].(string),
				Role:     claims["role"].(string),
			}
		},
		Authenticator: func(c *gin.Context) (interface{}, error) {
			var user vo.User
			if err := c.ShouldBind(&user); err != nil {
				return "", jwt.ErrMissingLoginValues
			}

			staff, err := auth.UserDao.FindUser(user)

			if err != nil || staff == nil {
				return nil, jwt.ErrFailedAuthentication
			}

			return staff, nil

		},
		Authorizator: func(data interface{}, c *gin.Context) bool {
			if v, ok := data.(*models.UserPlayer); ok && (common.Contains(roles, v.Role) || v.Role == "ADMIN") {
				c.Keys["users"] = v
				return true
			}
			return false
		},
		Unauthorized: func(context *gin.Context, code int, message string) {
			context.JSON(code, &common.Response{Code: code, Data: map[string]interface{}{
				"error": message,
			}})
		},
		TokenLookup:   "header: Authorization, query: token, cookie: jwt",
		TokenHeadName: "Bearer",
		TimeFunc:      time.Now,
		LoginResponse: func(context *gin.Context, code int, token string, expire time.Time) {
			context.JSON(code, &common.Response{Code: code, Data: map[string]interface{}{
				"token":  token,
				"expire": expire.Format(time.RFC1123),
			}})
		},
		RefreshResponse: func(context *gin.Context, code int, token string, expire time.Time) {
			context.JSON(code, &common.Response{Code: code, Data: map[string]interface{}{
				"token":  token,
				"expire": expire.Format(time.RFC1123),
			}})
		},
	}

	return middle
}

func (auth AuthServiceImpl) MiddlewareFunc(roles ...string) gin.HandlerFunc {

	if roles == nil || len(roles) == 0 {
		roles = make([]string, 0)
		roles = append(roles, "ADMIN")
	}

	middle := auth.CreateMiddleware(roles...)

	return middle.MiddlewareFunc()
}

func (auth AuthServiceImpl) CreateJwt(user vo.User, c *gin.Context) {
	middle := auth.CreateMiddleware()
	middle.Authenticator = func(c *gin.Context) (i interface{}, e error) {
		staff, err := auth.UserDao.FindUser(user)

		if err != nil || staff == nil {
			return nil, jwt.ErrFailedAuthentication
		}

		return staff, nil
	}

	middle.LoginHandler(c)
}
