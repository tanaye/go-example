package impl

import (
	"errors"
	"github.com/dgrijalva/jwt-go"
	"go-example/common"
	"go-example/dao"
	"go-example/dao/impl"
	"go-example/models"
	"go-example/models/vo"
	"strconv"
	"time"
)

var (
	ADMIN_USER  = 1
	NORMAL_USER = 2
	FB_USER     = 3
)

// UserServiceImpl : Implementation of IUserService
type UserServiceImpl struct {
	StaffDao dao.IUserDao
}

// NewUserService : Contructor new object
func NewUserService() *UserServiceImpl {
	var staffDao dao.IUserDao = impl.UserDaoImpl{}
	return &UserServiceImpl{StaffDao: staffDao}
}

// GetAllUser : Get User have role staff
func (service UserServiceImpl) GetAllUser() ([]models.UserPlayer, error) {
	return service.StaffDao.GetAllUser()
}

func (service UserServiceImpl) CreateUser(u vo.User) (int64, error) {
	id, err := service.StaffDao.CreateUser(u)

	if err == nil {
		if _, err := service.StaffDao.CreateUserPreferences(id, NORMAL_USER); err != nil {
			return -1, err
		}
	}

	return id, err
}

func (service UserServiceImpl) FindUser(u vo.User) (*models.UserPlayer, error) {
	staff, err := service.StaffDao.FindUser(u)

	if err != nil {
		return nil, err
	}

	if staff == nil {
		err := errors.New("user not found")
		return nil, err
	}

	return staff, nil

}

func (service UserServiceImpl) CreateJwt(staff *models.UserPlayer) (map[interface{}]interface{}, error) {

	expireTime := time.Now().Add(time.Hour * time.Duration(common.GetAppConfiguration()["expire"].(int))).Unix()

	claims := &jwt.StandardClaims{
		ExpiresAt: time.Now().Add(time.Hour * time.Duration(expireTime)).Unix(),
		Issuer:    staff.Username,
		Id:        strconv.Itoa(staff.UserId),
		IssuedAt:  time.Now().Unix(),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS512, claims)
	tokenString, err := token.SignedString([]byte(common.GetAppConfiguration()["privateKey"].(string)))
	if err != nil {
		return nil, err
	}

	tokenMap := map[interface{}]interface{}{}

	tokenMap["token"] = tokenString
	tokenMap["expired"] = expireTime

	return tokenMap, nil
}
