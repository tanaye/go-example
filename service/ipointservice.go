package service

import (
	"go-example/models"
	"go-example/models/vo"
)

type IPointService interface {
	GetTopPoint() ([]models.Point, error)

	Create(point vo.Point) (bool, error)
}
