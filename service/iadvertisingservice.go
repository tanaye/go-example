package service

import "go-example/models/vo"

type IAdvertisingService interface {
	GetListAdvs() ([]string, error)

	GetDetailAdvs() ([]vo.Advertising, error)

	CreateAdvertising(advertising vo.Advertising) (bool, error)

	DeleteAdvertising(i int) (bool, error)

	UpdateOrderingAdv(advertising vo.Advertising) (bool, error)
}
