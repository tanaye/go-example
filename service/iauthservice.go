package service

import (
	"github.com/appleboy/gin-jwt"
	"github.com/gin-gonic/gin"
	"go-example/models/vo"
)

type IAuthService interface {
	CreateMiddleware(roles ...string) jwt.GinJWTMiddleware

	MiddlewareFunc(roles ...string) gin.HandlerFunc

	CreateJwt(user vo.User, c *gin.Context)
}
