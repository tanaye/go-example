FROM golang:alpine
ADD . /go/src/go-example
WORKDIR /go/src/go-example
EXPOSE 8080
CMD ["go", "run", "main.go"]
