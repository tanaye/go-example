package router

import (
	"go-example/common"
	"go-example/controller"

	"github.com/gin-gonic/gin"
)

// Register : register url api
func Register() {

	userController := controller.NewUserController()
	authController := controller.NewAuthController()
	advController := controller.NewAdvertisingController()
	pointController := controller.NewPointController()

	engine := gin.New()

	engine.GET("/advs", advController.GetAdvs)

	// Routes do not authenticated
	authGroup := engine.Group("/auth")
	authGroup.POST("/login", authController.LoginHandler)
	authGroup.POST("/register", authController.Register)
	authGroup.GET("/refresh_token", authController.RefreshHandler)

	rolesGroup := common.GetRoleGroup()

	// Routes authenticated
	data := engine.Group("/data")
	data.GET("/point", pointController.GetTopPoint)
	data.Use(authController.AuthService.MiddlewareFunc(rolesGroup["user"]...))
	{
		data.POST("/point", pointController.CreatePoint)
		data.GET("/my-info", userController.GetMyInfo)
	}

	admin := engine.Group("/admin")
	admin.Use(authController.AuthService.MiddlewareFunc())
	{
		admin.POST("/adv", advController.CreateAdvertising)
		admin.PUT("/adv-ordering", advController.UpdateOrderingAdv)
		admin.GET("/advs", advController.GetDetailAdvs)
		admin.DELETE("/adv", advController.DeleteAdvertising)

		admin.GET("/users", userController.GetUsers)
	}

	_ = engine.Run()

}
