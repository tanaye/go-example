package common

import (
	"encoding/json"
	"go-example/models"
	"os"
)

// GetDBConfiguration : Create mysql databse connection & return models/Dbcnf.go
func GetDBConfiguration() (models.Dbconf, error) {
	config := models.Dbconf{}
	file, err := os.Open("./confdb.json")
	if err != nil {
		return config, err
	}
	defer file.Close()
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&config)
	if err != nil {
		return config, err
	}

	return config, nil
}

func GetAppConfiguration() map[string]interface{} {
	var objMap map[string]interface{}

	file, err := os.Open("./confapp.json")
	if err != nil {
		return objMap
	}

	defer file.Close()

	decoder := json.NewDecoder(file)
	err = decoder.Decode(&objMap)
	return objMap
}

func GetRoleGroup() map[string][]string {
	appConf := GetAppConfiguration()
	roles := appConf["roles"].(map[string]interface{})

	roleGroup := make(map[string][]string)

	for k, v := range roles {
		values := v.([]interface{})
		r := make([]string, 0)
		for _, element := range values {
			r = append(r, element.(string))
		}

		roleGroup[k] = r
	}

	return roleGroup
}

func Contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}
