package common

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

// Connection : Base struct to
type Connection struct {
}

func (c *Connection) getConn() (*sql.DB, error) {
	config, err := GetDBConfiguration()
	if err != nil {
		log.Fatalln(err)
		return nil, err
	}

	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?tls=false&autocommit=true",
		config.User, config.Password, config.Server, config.Port, config.Database)
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatalln(err)
		return nil, err
	}

	return db, err
}

func (c *Connection) Select(query string, params ...interface{}) (*sql.Rows, error) {
	db, err := c.getConn()
	if err != nil {
		return nil, err
	}
	defer db.Close()

	rows, err := db.Query(query, params...)
	if err != nil {
		return nil, err
	}

	return rows, nil
}

// Return lasted id
func (c *Connection) Insert(query string, params ...interface{}) (int64, error) {
	db, err := c.getConn()

	if err != nil {
		return -1, err
	}

	defer db.Close()

	if tx, err := db.Begin(); err != nil {
		return -1, err
	} else {
		res, err := tx.Exec(query, params...)
		if err != nil {
			_ = tx.Rollback()
			return -1, err
		} else {
			if id, err := res.LastInsertId(); err != nil {
				return -1, err
			} else {
				_ = tx.Commit()
				return id, nil
			}
		}
	}
}

// Return total row effect
func (c *Connection) Update(query string, params ...interface{}) (int64, error) {
	db, err := c.getConn()

	if err != nil {
		return -1, err
	}

	defer db.Close()

	if tx, err := db.Begin(); err != nil {
		return -1, err
	} else {
		res, err := tx.Exec(query, params...)
		if err != nil {
			_ = tx.Rollback()
			return -1, err
		} else {
			if id, err := res.RowsAffected(); err != nil {
				return -1, err
			} else {
				_ = tx.Commit()
				return id, nil
			}
		}
	}
}
