package common

type Response struct {
	Code int
	Data map[string]interface{}
}
